<?php

/**
 * Add shortcode, where add div block for map
 */
function map_shortcode_function() {
	echo '<div id="general_map"></div>';
}

add_shortcode( 'map_shortcode', 'map_shortcode_function' );
