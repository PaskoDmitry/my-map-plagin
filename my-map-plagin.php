<?php
/*
Plugin Name: My Map Plagin
Description: Work with Google Map API
Version: 1.0
Author: Pasko Dmitry
*/
?>
<?php
/*  Copyright 2017  Pasko_dmitry  (email: PaskoDima@meta.ua)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
?>
<?php
require_once dirname( __FILE__ ) . '/map-shortcode.php';
define( 'MY_PLAGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'MY_PLAGIN__URL', plugin_dir_url( __FILE__ ) );

/**
 * Create post type shops
 */
function wpt_shop_posttype() {
	register_post_type( 'shops',
		array(
			'labels'               => array(
				'name'               => __( 'Shops' ),
				'singular_name'      => __( 'Shop' ),
				'add_new'            => __( 'Add new' ),
				'add_new_item'       => __( 'Add new shop' ),
				'edit_item'          => __( 'Edit shop' ),
				'new_item'           => __( 'New Shop' ),
				'view_item'          => __( 'View shop' ),
				'search_items'       => __( 'Search shop' ),
				'not_found'          => __( 'Shops is not found' ),
				'not_found_in_trash' => __( 'Shops is not found in trash' )
			),
			'public'               => true,
			'supports'             => array( 'title', 'editor', 'thumbnail', 'comments' ),
			'capability_type'      => 'post',
			'rewrite'              => array( "slug" => "shops" ), // Permalinks format
			'menu_position'        => 5,
			'register_meta_box_cb' => 'add_shops_metaboxes'
		)
	);
}

add_action( 'init', 'wpt_shop_posttype' );

add_action( 'add_meta_boxes', 'add_shops_metaboxes' );

/**
 * Add the Shops Meta Boxes
 */
function add_shops_metaboxes() {
	add_meta_box( 'wpt_shops_info', __( 'Shop information' ), 'wpt_shops_info', 'shops', 'normal', 'high' );
}

/**
 * Show the meta field data in the admin panel
 */
function wpt_shops_info() {
	global $post;

	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="shopmeta_noncename" id="shopmeta_noncename" value="' .
	     wp_create_nonce( plugin_basename( __FILE__ ) ) . '" />';

	// Get the location data if its already been entered
	$name        = get_post_meta( $post->ID, '_name', true );
	$description = get_post_meta( $post->ID, '_description', true );
	$address     = get_post_meta( $post->ID, '_address', true );


	// Echo out the field
	echo '<p>' . __( 'Shop name: ' ) . '</p>';
	echo '<input type="text" name="_name" value="' . $name . '" class="widefat" />';
	echo '<p>' . __( 'Shop description: ' ) . '</p>';
	echo '<input type="text" name="_description" value="' . $description . '" class="widefat" />';
	echo '<p>' . __( 'Shop address: ' ) . '</p>';
	echo '<input type="text" name="_address" value="' . $address . '" class="widefat" />';

}


/**
 * Save the Metabox Data
 *
 * @param $post_id
 * @param $post
 */
function wpt_save_shops_meta( $post_id, $post ) {

	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( ! wp_verify_nonce( $_POST['shopmeta_noncename'], plugin_basename( __FILE__ ) ) ) {
		return $post->ID;
	}

	// Is the user allowed to edit the post or page?
	if ( ! current_user_can( 'edit_post', $post->ID ) ) {
		return $post->ID;
	}

	// Send a geocoding request
	$mapinfojson = file_get_contents( 'https://maps.googleapis.com/maps/api/geocode/json?address=' . str_replace( ' ', '+', $_POST['_address'] ) . '&key=AIzaSyDqG4R9JbhUvZd_alhc27It4k0MsKzvL4w' );
	$mapinfo     = json_decode( $mapinfojson );
	// OK, we're authenticated: we need to find and save the data
	// We'll put it into an array to make it easier to loop though.

	$shop_meta['_name']        = $_POST['_name'];
	$shop_meta['_description'] = $_POST['_description'];
	$shop_meta['_address']     = $_POST['_address'];
	$shop_meta['_lat']         = ( $mapinfo->results[0]->geometry->location->lat );
	$shop_meta['_lng']         = ( $mapinfo->results[0]->geometry->location->lng );


	foreach ( $shop_meta as $key => $value ) { // Cycle through the $events_meta array!
		if ( $post->post_type == 'revision' ) {
			return;
		} // Don't store custom data twice
		$value = implode( ',', (array) $value ); // If $value is an array, make it a CSV (unlikely)
		update_post_meta( $post->ID, $key, $value );

		if ( ! $value ) {
			delete_post_meta( $post->ID, $key );
		} // Delete if blank
	}
}

add_action( 'save_post', 'wpt_save_shops_meta', 1, 2 ); // save the custom fields


/**
 * I connect plugin js file
 */
function wp_mymap_plagin_scripts() {
	global $post;
	$src = plugins_url( '/my-map-plagin.js?42271', __FILE__ );
	wp_enqueue_script( 'mymap_plagin-script', $src, array( 'jquery' ) );
	$map_params = [
		'lat'     => ( get_post_meta( $post->ID, '_lat', true ) ),
		'lng'     => ( get_post_meta( $post->ID, '_lng', true ) ),
		'address' => ( get_post_meta( $post->ID, '_address', true ) )
	];
	wp_localize_script( 'mymap_plagin-script', 'map_params', $map_params );
}

add_action( 'wp_enqueue_scripts', 'wp_mymap_plagin_scripts' );

/**
 * Loads the API from the specified URL
 */
function wp_google_scripts() {
	$src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDqG4R9JbhUvZd_alhc27It4k0MsKzvL4w';
	wp_enqueue_script( 'google-script', $src, array( 'jquery', 'mymap_plagin-script' ) );

}

add_action( 'wp_enqueue_scripts', 'wp_google_scripts' );

/**
 * I connect the script and add additional data before the script
 */
function wp_shops_scripts() {
	$src = plugins_url( 'addresses.js?1448', __FILE__ );
	wp_enqueue_script( 'addresses-script', $src, array( 'jquery' ) );

	$args      = array(
		'post_type' => 'Shops'
	);
	$query     = new WP_Query();
	$my_posts  = $query->query( $args );
	$addresses = [];
	foreach ( $my_posts as $my_post ) {
		$addresses[] = [
			'lat'     => ( get_post_meta( $my_post->ID, '_lat', true ) ),
			'lng'     => ( get_post_meta( $my_post->ID, '_lng', true ) ),
			'address' => ( get_post_meta( $my_post->ID, '_address', true ) ),
			'name'    => ( get_post_meta( $my_post->ID, '_name', true ) )
		];
	}
	wp_localize_script( 'addresses-script', 'addresses', $addresses );
}

add_action( 'wp_enqueue_scripts', 'wp_shops_scripts' );

/**
 * Connect css  file of a plugin
 */
function wp_mymap_plagin_style() {
	// Register the style:
	wp_register_style( 'google-map-style', plugins_url( '/google-map-style.css?4221', __FILE__ ), array(), '20120208', 'all' );

	// enqueue the style:
	wp_enqueue_style( 'google-map-style' );
}

add_action( 'wp_enqueue_scripts', 'wp_mymap_plagin_style' );
