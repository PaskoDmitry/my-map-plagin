jQuery(document).ready(function ($) {
    if ($("div").is("#map")) {

        var lat = +(map_params.lat);
        var lng = +(map_params.lng);
        var address = map_params.address;

        var location = {lat: lat, lng: lng};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: location
        });
        var contentString = '<div id="mapInfo">Address: ' + address + '</div>';
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });
    }
});
