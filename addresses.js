jQuery(document).ready(function ($) {
    if ($("div").is("#general_map")) {
        for (var i in addresses) {

            var location = [];
            location[addresses[i].name] = {lat: +(addresses[i].lat), lng: +(addresses[i].lng)};
            var map
            if (map == undefined) {
                var map = new google.maps.Map(document.getElementById('general_map'), {
                    zoom: 1,
                    center: (location[addresses[i].name])
                });
            }
            var contentString = [];
            contentString[addresses[i]] = '<div id="mapInfo">Address: ' + addresses[i].address + '</div>';

            var infowindow = new google.maps.InfoWindow();

            var marker = new google.maps.Marker({
                position: location[addresses[i].name],
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function (marker, contentString, infowindow) {
                return function () {
                    infowindow.setContent(contentString[addresses[i]]);
                    infowindow.open(map, marker);
                };
            })(marker, contentString, infowindow));
        }
    }
});
